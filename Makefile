build:
	go build -o ./bin/kittens ./cmd/kittens

gen-graph:
	go run github.com/99designs/gqlgen

test:
	docker-compose -f docker-compose-test.yaml up --build --abort-on-container-exit
	docker-compose -f docker-compose-test.yaml down --volumes
