package main

import (
	"context"
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"log"
	"log/slog"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/kousikmitra/kitten-tracker/db"
	"github.com/kousikmitra/kitten-tracker/pkg/animal"
	"github.com/kousikmitra/kitten-tracker/pkg/animal/animalimpl"
	"github.com/kousikmitra/kitten-tracker/pkg/api"
	"github.com/kousikmitra/kitten-tracker/pkg/config"
	"github.com/kousikmitra/kitten-tracker/pkg/email"
	"github.com/kousikmitra/kitten-tracker/pkg/user/userimpl"
)

var sqlDB *sql.DB
var cfg *config.Config

func init() {
	var err error

	godotenv.Load()
	cfg, err = config.LoadFromENV()
	if err != nil {
		log.Fatalf("failed to load config from env: %s", err.Error())
	}

	sqlDB, err = db.NewDB(cfg.Database)
	if err != nil {
		log.Fatal(err)
	}
}

type Runner interface {
	Init([]string) error
	Run() error
	Name() string
}

func NewMigrateCommand() *MigrateCommand {
	gc := &MigrateCommand{
		fs: flag.NewFlagSet("migrate", flag.ContinueOnError),
	}

	return gc
}

type MigrateCommand struct {
	fs *flag.FlagSet
}

func (g *MigrateCommand) Name() string {
	return g.fs.Name()
}

func (g *MigrateCommand) Init(args []string) error {
	return g.fs.Parse(args)
}

func (g *MigrateCommand) Run() error {
	if err := db.MigrateUp(sqlDB); err != nil {
		return err
	}
	return nil
}

func NewServerCommand() *ServerCommand {
	gc := &ServerCommand{
		fs: flag.NewFlagSet("server", flag.ContinueOnError),
	}

	return gc
}

type ServerCommand struct {
	fs *flag.FlagSet
}

func (g *ServerCommand) Name() string {
	return g.fs.Name()
}

func (g *ServerCommand) Init(args []string) error {
	return g.fs.Parse(args)
}

func (g *ServerCommand) Run() error {

	notifer := animal.NewNotifierService(slog.Default(), email.NewLogMailer(slog.Default()))
	userStore := userimpl.NewSQLUserStore(sqlDB)
	userService := userimpl.NewUserService(cfg, userStore)
	animalStore := animalimpl.NewAnimalSQLStore(sqlDB)
	animalService := animalimpl.NewAnimalService(animalStore, notifer)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go notifer.Run(ctx)

	server := api.NewServer(userService, animalService)

	log.Printf("Connect to http://%s:%s/ for GraphQL playground", cfg.Server.Addr, cfg.Server.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%s", cfg.Server.Addr, cfg.Server.Port), server.Handler()))
	return nil
}

func root(args []string) error {
	if len(args) < 1 {
		return errors.New("must pass a subcommand")
	}

	cmds := []Runner{
		NewMigrateCommand(),
		NewServerCommand(),
	}

	subcommand := os.Args[1]

	for _, cmd := range cmds {
		if cmd.Name() == subcommand {
			cmd.Init(os.Args[2:])
			return cmd.Run()
		}
	}

	return fmt.Errorf("unknown subcommand: %s", subcommand)
}

func main() {
	if err := root(os.Args[1:]); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
