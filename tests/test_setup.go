package tests

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"testing"

	"github.com/golang-migrate/migrate/v4"
	"github.com/kousikmitra/kitten-tracker/db"
	"github.com/kousikmitra/kitten-tracker/pkg/api"
	"github.com/kousikmitra/kitten-tracker/pkg/config"
	"github.com/kousikmitra/kitten-tracker/pkg/user/userimpl"
	"github.com/stretchr/testify/require"
)

func initTestSetup(t *testing.T) string {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	host := os.Getenv("DB_HOST")
	if host == "" {
		host = "localhost"
	}
	// create and cleanup test db
	dbCfg := config.DatabaseConfig{
		Host:     host,
		Port:     "5432",
		Username: "postgres",
		Password: "postgres",
		Name:     "kitten_tracker_test",
	}
	cfg := &config.Config{
		Database:  dbCfg,
		JWTSecret: "secret",
	}
	dbConn, err := db.NewDB(dbCfg)
	if err != nil {
		log.Fatal("error connecting to test db: ", err)
	}
	t.Cleanup(func() {
		if err := dbConn.Close(); err != nil {
			log.Fatal("error closing conn: ", err)
		}
	})
	err = db.MigrateUp(dbConn)
	if err != nil && err != migrate.ErrNoChange {
		log.Fatal("error migrating db: ", err)
	}
	t.Cleanup(func() {
		if err := db.MigrateDown(dbConn); err != nil {
			log.Fatal("error reverting migrations: ", err)
		}

	})
	userStore := userimpl.NewSQLUserStore(dbConn)
	userService := userimpl.NewUserService(cfg, userStore)
	apiServer := api.NewServer(userService, nil)

	l, err := net.Listen("tcp", "127.0.0.1:0")
	require.NoError(t, err)
	hs := http.Server{Handler: apiServer.Handler()}
	go func() {
		hs.Serve(l)
	}()

	t.Cleanup(func() {
		hs.Shutdown(context.Background())
	})
	t.Cleanup(func() {

	})
	return l.Addr().String()
}
