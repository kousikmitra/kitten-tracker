package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/kousikmitra/kitten-tracker/pkg/user"
	"github.com/stretchr/testify/require"
)

func Test_LoginUserAPI(t *testing.T) {
	addr := initTestSetup(t)
	payload := &bytes.Buffer{}

	// Create user
	createUserCmd := user.CreateUserCmd{
		Email:    "test@test.com",
		Password: "password",
	}
	err := json.NewEncoder(payload).Encode(createUserCmd)
	require.NoError(t, err)
	resp, err := http.Post(fmt.Sprintf("http://%s/api/user", addr), "application/json", payload)
	require.NoError(t, err)
	require.Equal(t, 201, resp.StatusCode)
	t.Cleanup(func() {
		require.NoError(t, resp.Body.Close())
	})

	b, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	usr := &user.User{}
	err = json.Unmarshal(b, usr)
	require.NoError(t, err)
	require.EqualValues(t, createUserCmd.Email, usr.Email)

	// Login user
	loginCmd := user.LoginRequest{
		Login:    usr.Email,
		Password: createUserCmd.Password,
	}

	err = json.NewEncoder(payload).Encode(loginCmd)
	require.NoError(t, err)
	resp, err = http.Post(fmt.Sprintf("http://%s/api/login", addr), "application/json", payload)
	require.NoError(t, err)
	require.Equal(t, 200, resp.StatusCode)
	t.Cleanup(func() {
		require.NoError(t, resp.Body.Close())
	})

	b, err = io.ReadAll(resp.Body)
	require.NoError(t, err)
	token := &user.AuthToken{}
	err = json.Unmarshal(b, token)
	require.NoError(t, err)
	require.NotEmpty(t, token.Token)
}
