package graph

import (
	"github.com/kousikmitra/kitten-tracker/pkg/animal"
	"github.com/kousikmitra/kitten-tracker/pkg/user"
)

func NewResolver(userService user.Service, animalService animal.Service) *Resolver {
	return &Resolver{
		UserService:   userService,
		AnimalService: animalService,
	}
}

type Resolver struct {
	UserService   user.Service
	AnimalService animal.Service
}
