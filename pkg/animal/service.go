package animal

import (
	"context"
	"time"

	"github.com/kousikmitra/kitten-tracker/pkg/email"
)

type Service interface {
	CreateAnimal(ctx context.Context, cmd CreateAnimalCmd) (*Animal, error)
	ListAnimals(ctx context.Context, q ListAnimalsQuery) ([]Animal, error)
	AddAnimalSighting(ctx context.Context, cmd CreateSightingCmd) (*AnimalSighting, error)
	ListAnimalSightings(ctx context.Context, q AnimalSightingsQuery) ([]AnimalSighting, error)
}

type Store interface {
	CreateAnimal(ctx context.Context, animal Animal) (*Animal, error)
	ListAnimals(ctx context.Context, q ListAnimalsQuery) ([]Animal, error)
	GetAnimalByID(ctx context.Context, id int64) (*Animal, error)
	CreateAnimalSighting(ctx context.Context, sighting AnimalSighting) (*AnimalSighting, error)
	ListAnimalSightings(ctx context.Context, q AnimalSightingsQuery) ([]AnimalSighting, error)
	DistanceFromLastSighting(ctx context.Context, animalID int64, lat, lon float64) (time.Time, float64, error)
	GetPrevousReporters(ctx context.Context, animalID int64) ([]string, error)
}

type SightingNotifer interface {
	Notify(ctx context.Context, sightings *email.Email)
}
