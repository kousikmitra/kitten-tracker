package animal

import (
	"errors"
	"time"
)

const (
	MaxLastSightingRadiusKm float64 = 5
)

var (
	ErrSigtingAlreadyReported = errors.New("someone has already reported this sighting within 5 km radius")
	ErrNoPreviousSighting     = errors.New("no previous sighting has been reported")
)

type Animal struct {
	ID          int64
	Name        string
	DateOfBirth time.Time
	LastSeenAt  *time.Time
	LastLocLat  *float64
	LastLocLon  *float64
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type AnimalSighting struct {
	ID         int64
	AnimalID   int64
	ReportedBy int64
	Lat        float64
	Lon        float64
	Timestamp  time.Time
}

type CreateAnimalCmd struct {
	Name        string
	DateOfBirth time.Time
	LastSeenAt  *time.Time
	LastLocLat  *float64
	LastLocLon  *float64
}

type ListAnimalsQuery struct {
	Skip  int
	Limit int
}

type CreateSightingCmd struct {
	AnimalID   int64
	ReportedBy int64
	Lat        float64
	Lon        float64
	Timestamp  time.Time
}

type AnimalSightingsQuery struct {
	Skip  int
	Limit int
}
