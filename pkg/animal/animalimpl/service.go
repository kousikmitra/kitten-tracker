package animalimpl

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"time"

	"github.com/kousikmitra/kitten-tracker/pkg/animal"
	"github.com/kousikmitra/kitten-tracker/pkg/email"
)

const KittenTrackerEmail = "notifications@kitten-tracker.com"

type animalService struct {
	store    animal.Store
	notifier animal.SightingNotifer
}

func NewAnimalService(store animal.Store, notifier animal.SightingNotifer) animal.Service {
	return &animalService{store, notifier}
}

func (as animalService) CreateAnimal(ctx context.Context, cmd animal.CreateAnimalCmd) (*animal.Animal, error) {
	aml := animal.Animal{
		Name:        cmd.Name,
		DateOfBirth: cmd.DateOfBirth,
		LastSeenAt:  cmd.LastSeenAt,
		LastLocLat:  cmd.LastLocLat,
		LastLocLon:  cmd.LastLocLon,
	}
	return as.store.CreateAnimal(ctx, aml)
}

func (as animalService) ListAnimals(ctx context.Context, q animal.ListAnimalsQuery) ([]animal.Animal, error) {
	return as.store.ListAnimals(ctx, q)
}

func (as animalService) AddAnimalSighting(ctx context.Context, cmd animal.CreateSightingCmd) (*animal.AnimalSighting, error) {
	lastSeenAt, distance, err := as.store.DistanceFromLastSighting(ctx, cmd.AnimalID, cmd.Lat, cmd.Lon)
	if err != nil && err != animal.ErrNoPreviousSighting {
		return nil, fmt.Errorf("error checking distance from last sighting: %w", err)
	}
	if cmd.Timestamp.Before(lastSeenAt) {
		return nil, fmt.Errorf("sighting timestamp is before %s", lastSeenAt.String())
	}

	if err != animal.ErrNoPreviousSighting && distance < animal.MaxLastSightingRadiusKm {
		return nil, animal.ErrSigtingAlreadyReported
	}

	amlSighting := animal.AnimalSighting{
		AnimalID:   cmd.AnimalID,
		Lat:        cmd.Lat,
		Lon:        cmd.Lon,
		Timestamp:  cmd.Timestamp,
		ReportedBy: cmd.ReportedBy,
	}

	sighting, err := as.store.CreateAnimalSighting(ctx, amlSighting)
	if err != nil {
		return nil, fmt.Errorf("error adding sighting record: %w", err)
	}

	msg, err := as.buildEmailMsg(ctx, sighting)
	if err != nil {
		slog.With("error", err).Error("unable to send tiger sighting notification")
	} else {
		as.notifier.Notify(ctx, msg)
	}

	return sighting, nil
}

func (as animalService) buildEmailMsg(ctx context.Context, sighting *animal.AnimalSighting) (*email.Email, error) {
	aml, err := as.store.GetAnimalByID(ctx, sighting.AnimalID)
	if err != nil {
		return nil, fmt.Errorf("error fetching animal record: %w", err)
	}
	emails, err := as.store.GetPrevousReporters(ctx, sighting.AnimalID)
	if err != nil {
		return nil, fmt.Errorf("error fetching users emails: %w", err)
	}
	body := fmt.Sprintf("Hi, Tiger %s has been sighted at %f,%f around %s.", aml.Name, sighting.Lat, sighting.Lon, sighting.Timestamp.Format(time.RFC850))
	msg := &email.Email{From: KittenTrackerEmail, To: emails, Content: body}
	return msg, nil
}

func (as animalService) ListAnimalSightings(ctx context.Context, q animal.AnimalSightingsQuery) ([]animal.AnimalSighting, error) {
	return as.store.ListAnimalSightings(ctx, q)
}

type animalSQLStore struct {
	db *sql.DB
}

func NewAnimalSQLStore(db *sql.DB) animal.Store {
	return &animalSQLStore{db}
}

func (as animalSQLStore) CreateAnimal(ctx context.Context, aml animal.Animal) (*animal.Animal, error) {
	var insertedID int64
	err := as.db.QueryRowContext(
		ctx,
		"INSERT INTO animals (name, date_of_birth, last_seen_at, last_loc_lat, last_loc_lon) VALUES ($1, $2, $3, $4, $5) RETURNING id",
		aml.Name, aml.DateOfBirth, aml.LastSeenAt, aml.LastLocLat, aml.LastLocLon,
	).Scan(&insertedID)
	if err != nil {
		return nil, fmt.Errorf("error inserting user in db: %w", err)
	}

	aml.ID = insertedID

	return &aml, nil
}

func (as animalSQLStore) ListAnimals(ctx context.Context, q animal.ListAnimalsQuery) ([]animal.Animal, error) {
	sql := "SELECT id, name, date_of_birth, last_seen_at, last_loc_lat, last_loc_lon, created_at, updated_at  FROM animals ORDER BY last_seen_at DESC OFFSET $1 LIMIT $2"
	args := []any{q.Skip, q.Limit}
	rows, err := as.db.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("error fetching animal records from db: %w", err)
	}
	animals := []animal.Animal{}
	for rows.Next() {
		var aml animal.Animal
		rows.Scan(&aml.ID, &aml.Name, &aml.DateOfBirth, &aml.LastSeenAt, &aml.LastLocLat, &aml.LastLocLon, &aml.CreatedAt, &aml.UpdatedAt)
		animals = append(animals, aml)
	}
	return animals, nil
}

func (as animalSQLStore) GetAnimalByID(ctx context.Context, id int64) (*animal.Animal, error) {
	aml := &animal.Animal{}
	sql := "SELECT id, name, date_of_birth, last_seen_at, last_loc_lat, last_loc_lon, created_at, updated_at  FROM animals WHERE id = $1"
	args := []any{id}
	err := as.db.QueryRowContext(ctx, sql, args...).Scan(&aml.ID, &aml.Name, &aml.DateOfBirth, &aml.LastSeenAt, &aml.LastLocLat, &aml.LastLocLon, &aml.CreatedAt, &aml.UpdatedAt)
	if err != nil {
		return nil, fmt.Errorf("error fetching animal record from db: %w", err)
	}

	return aml, nil
}

func (as animalSQLStore) CreateAnimalSighting(ctx context.Context, amlSighting animal.AnimalSighting) (*animal.AnimalSighting, error) {
	var insertedID int64
	txn, err := as.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to begin transaction: %w", err)
	}
	defer txn.Rollback()
	err = txn.QueryRowContext(
		ctx,
		"INSERT INTO animal_sightings (animal_id, reported_by, lat, lon, ts) VALUES ($1, $2, $3, $4, $5) RETURNING id",
		amlSighting.AnimalID, amlSighting.ReportedBy, amlSighting.Lat, amlSighting.Lon, amlSighting.Timestamp,
	).Scan(&insertedID)
	if err != nil {
		return nil, fmt.Errorf("error inserting user in db: %w", err)
	}

	updateSQL := `
	UPDATE animals SET
		last_seen_at = last_sigting.ts,
		last_loc_lat = last_sigting.lat,
		last_loc_lon = last_sigting.lon,
		updated_at = NOW()
	FROM (SELECT animal_id, lat, lon, ts FROM animal_sightings WHERE animal_id = $1 ORDER BY ts DESC LIMIT 1) as last_sigting
	WHERE id = last_sigting.animal_id;
	`

	if _, err := txn.Exec(updateSQL, amlSighting.AnimalID); err != nil {
		return nil, fmt.Errorf("error updating last seen details: %w", err)
	}

	if err := txn.Commit(); err != nil {
		return nil, fmt.Errorf("error commiting transaction: %w", err)
	}

	amlSighting.ID = insertedID

	return &amlSighting, nil
}

func (as animalSQLStore) ListAnimalSightings(ctx context.Context, q animal.AnimalSightingsQuery) ([]animal.AnimalSighting, error) {
	sql := "SELECT id, animal_id, lat, lon, ts  FROM animal_sightings ORDER BY ts DESC OFFSET $1 LIMIT $2"
	args := []any{q.Skip, q.Limit}
	rows, err := as.db.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("error fetching animal records from db: %w", err)
	}
	sightings := []animal.AnimalSighting{}
	for rows.Next() {
		var sighting animal.AnimalSighting
		rows.Scan(&sighting.ID, &sighting.AnimalID, &sighting.Lat, &sighting.Lon, &sighting.Timestamp)
		sightings = append(sightings, sighting)
	}
	return sightings, nil
}

func (as animalSQLStore) DistanceFromLastSighting(ctx context.Context, animalID int64, lat, lon float64) (time.Time, float64, error) {
	var distance float64
	var lastSeenAt time.Time
	query := "SELECT last_seen_at, calculate_distance(last_loc_lat, last_loc_lon, $1, $2, 'K') FROM animals WHERE id = $3 and last_loc_lat IS NOT NULL and last_loc_lon IS NOT NULL"
	args := []any{lat, lon, animalID}
	err := as.db.QueryRowContext(ctx, query, args...).Scan(&lastSeenAt, &distance)
	if err != nil {
		if err == sql.ErrNoRows {
			return lastSeenAt, distance, animal.ErrNoPreviousSighting
		}
		return lastSeenAt, distance, fmt.Errorf("error fetching animal records from db: %w", err)
	}

	return lastSeenAt, distance, nil
}

func (as animalSQLStore) GetPrevousReporters(ctx context.Context, animalID int64) ([]string, error) {
	sql := `
	SELECT DISTINCT email FROM users u
	INNER JOIN animal_sightings as sightings ON sightings.reported_by = u.id
	WHERE sightings.animal_id = $1
	`
	args := []any{animalID}
	rows, err := as.db.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("error fetching animal records from db: %w", err)
	}
	emails := []string{}
	for rows.Next() {
		var emailID string
		rows.Scan(&emailID)
		emails = append(emails, emailID)
	}
	return emails, nil
}
