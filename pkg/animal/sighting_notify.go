package animal

import (
	"context"
	"log/slog"

	"github.com/kousikmitra/kitten-tracker/pkg/email"
)

type NotifierService struct {
	logger *slog.Logger
	mailer email.Mailer

	mailQ chan *email.Email
}

func NewNotifierService(logger *slog.Logger, mailer email.Mailer) *NotifierService {
	return &NotifierService{
		logger: logger.WithGroup("notifer"),
		mailer: mailer,
		mailQ:  make(chan *email.Email, 100), // hardcoded queue size to 100
	}
}

func (ns NotifierService) Notify(ctx context.Context, msg *email.Email) {
	ns.mailQ <- msg
}

func (ns NotifierService) Run(ctx context.Context) error {
	for {
		select {
		case msg := <-ns.mailQ:
			err := ns.sendEmail(ctx, msg)
			if err != nil {
				ns.logger.With("error", err).Error("something went wrong while sending sighting notigication")
			}
		case <-ctx.Done():
			ns.logger.Info("shutting down the notifer")
			return ctx.Err()
		}
	}
}

func (ns NotifierService) sendEmail(ctx context.Context, msg *email.Email) error {
	return ns.mailer.Send(ctx, *msg)
}
