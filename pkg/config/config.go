package config

import (
	"fmt"
	"os"
)

type DatabaseConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	Name     string
}

type ServerConfig struct {
	Addr string
	Port string
}

type Config struct {
	Server    ServerConfig
	Database  DatabaseConfig
	JWTSecret string
}

func LoadFromENV() (*Config, error) {
	cnf := &Config{}
	if err := cnf.loadServerConfig(); err != nil {
		return cnf, fmt.Errorf("error loading server config: %w", err)
	}
	if err := cnf.loadDatabaseConfig(); err != nil {
		return cnf, fmt.Errorf("error loading database config: %w", err)
	}

	cnf.JWTSecret = os.Getenv("JWT_SECRET")

	return cnf, nil
}

func (cnf *Config) loadServerConfig() error {
	cnf.Server = ServerConfig{
		Addr: os.Getenv("SERVER_ADDR"),
		Port: os.Getenv("SERVER_PORT"),
	}
	return nil
}

func (cnf *Config) loadDatabaseConfig() error {
	//TODO: add existing check
	cnf.Database = DatabaseConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		Name:     os.Getenv("DB_NAME"),
	}
	return nil
}
