package user

import (
	"context"
	"errors"
	"net/mail"
	"time"
)

var (
	ErrUserNotFound        = errors.New("user not found")
	ErrEmailRequired       = errors.New("user cannot be created without an email")
	ErrLoginHandleRequired = errors.New("missing login handle")
	ErrUserExists          = errors.New("user already exists with same login credentials")
	ErrInvalidEmail        = errors.New("invalid email address")
	ErrCredentialMismatch  = errors.New("login failed")
	ErrInvalidToken        = errors.New("invalid authentication token")
)

var (
	AuthUserCtxKey = struct{}{}
)

type User struct {
	ID             int64     `json:"id,omitempty"`
	Username       string    `json:"username,omitempty"`
	Email          string    `json:"email,omitempty"`
	HashedPassword string    `json:"-"`
	Salt           string    `json:"-"`
	CreatedAt      time.Time `json:"createdAt,omitempty"`
	UpdatedAt      time.Time `json:"updatedAt,omitempty"`
}

type CreateUserCmd struct {
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type LoginRequest struct {
	Login    string `json:"login,omitempty"`
	Password string `json:"password,omitempty"`
}

type AuthToken struct {
	Token string `json:"token,omitempty"`
}

func ValidEmail(email string) bool {
	emailAddress, err := mail.ParseAddress(email)
	return err == nil && emailAddress.Address == email
}

func LoggedInUser(ctx context.Context) *User {
	if usr, ok := ctx.Value(AuthUserCtxKey).(*User); ok {
		return usr
	}
	return nil
}
