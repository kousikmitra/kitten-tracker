package user

import "context"

type Service interface {
	CreateUser(ctx context.Context, cmd CreateUserCmd) (*User, error)
	Login(ctx context.Context, req LoginRequest) (*AuthToken, error)
	Authenticate(ctx context.Context, token string) (*User, error)
}

type Store interface {
	CreateUser(ctx context.Context, cmd User) (*User, error)
	GetByLoginHandle(ctx context.Context, login string) (*User, error)
	LoginConflict(ctx context.Context, username, email string) (bool, error)
}
