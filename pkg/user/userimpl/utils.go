package userimpl

import (
	"encoding/hex"
	"fmt"
	"math/rand"

	"golang.org/x/crypto/scrypt"
)

var alphanum = []rune("123467890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandomSequence(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = alphanum[rand.Intn(len(alphanum))]
	}
	return string(b)
}

func EncodePassword(password string, salt string) (string, error) {
	encPassword, err := scrypt.Key([]byte(password), []byte(salt), 1<<15, 8, 1, 32)
	if err != nil {
		return "", fmt.Errorf("error generating scrypt hash: %w", err)
	}
	return hex.EncodeToString(encPassword), nil
}
