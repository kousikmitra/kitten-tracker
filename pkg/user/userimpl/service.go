package userimpl

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/kousikmitra/kitten-tracker/pkg/config"
	"github.com/kousikmitra/kitten-tracker/pkg/user"
)

type userServiceImpl struct {
	jwtSecret []byte
	store     user.Store
}

func NewUserService(cfg *config.Config, store user.Store) user.Service {
	return &userServiceImpl{
		jwtSecret: []byte(cfg.JWTSecret),
		store:     store,
	}
}

func (us userServiceImpl) CreateUser(ctx context.Context, cmd user.CreateUserCmd) (*user.User, error) {
	if !user.ValidEmail(cmd.Email) {
		return nil, user.ErrInvalidEmail
	}

	if len(cmd.Password) == 0 {
		return nil, fmt.Errorf("password is required")
	}

	if len(cmd.Username) == 0 {
		cmd.Username = cmd.Email
	}
	conflict, err := us.store.LoginConflict(ctx, cmd.Username, cmd.Email)
	if err != nil {
		return nil, fmt.Errorf("error checking user login: %w", err)
	}
	if conflict {
		return nil, user.ErrUserExists
	}

	salt := RandomSequence(10)
	hashedPassword, err := EncodePassword(cmd.Password, salt)
	if err != nil {
		return nil, fmt.Errorf("error encoding password: %w", err)
	}

	usr := user.User{
		Username:       cmd.Username,
		Email:          cmd.Email,
		HashedPassword: hashedPassword,
		Salt:           salt,
	}

	return us.store.CreateUser(ctx, usr)
}

func (us userServiceImpl) Authenticate(ctx context.Context, tokenStr string) (*user.User, error) {
	keyFn := func(token *jwt.Token) (interface{}, error) {
		return us.jwtSecret, nil
	}
	token, err := jwt.Parse(tokenStr, keyFn)
	if err != nil {
		return nil, fmt.Errorf("error parsing token: %w", err)
	}

	if !token.Valid {
		return nil, user.ErrInvalidToken
	}

	sub, err := token.Claims.GetSubject()
	if err != nil {
		return nil, fmt.Errorf("unabled to retrive claims from token: %w", err)
	}

	return us.store.GetByLoginHandle(ctx, sub)
}

func (us userServiceImpl) Login(ctx context.Context, req user.LoginRequest) (*user.AuthToken, error) {
	if len(req.Login) == 0 {
		return nil, user.ErrLoginHandleRequired
	}

	usr, err := us.store.GetByLoginHandle(ctx, req.Login)
	if err != nil {
		return nil, fmt.Errorf("unable to get user detail from db: %w", err)
	}

	givenPassword, err := EncodePassword(req.Password, usr.Salt)
	if err != nil {
		return nil, fmt.Errorf("unable to encode given password: %w", err)
	}

	if usr.HashedPassword != givenPassword {
		return nil, user.ErrCredentialMismatch
	}

	token, err := us.generateJWTToken(usr)
	if err != nil {
		return nil, fmt.Errorf("error generating jwt token: %w", err)
	}
	return &user.AuthToken{Token: token}, nil
}

func (us userServiceImpl) generateJWTToken(usr *user.User) (string, error) {
	claims := jwt.RegisteredClaims{}
	claims.IssuedAt = jwt.NewNumericDate(time.Now())
	claims.ExpiresAt = jwt.NewNumericDate(claims.IssuedAt.Add(10 * time.Minute))
	claims.Subject = usr.Username

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(us.jwtSecret)
	if err != nil {
		return "", fmt.Errorf("error signing jwt token: %w", err)
	}

	return tokenStr, nil
}

type userStoreImpl struct {
	db *sql.DB
}

func NewSQLUserStore(db *sql.DB) user.Store {
	return &userStoreImpl{db}
}

func (us userStoreImpl) CreateUser(ctx context.Context, usr user.User) (*user.User, error) {
	var insertedID int64
	err := us.db.QueryRowContext(
		ctx,
		"INSERT INTO users (username, email, hashed_password, salt) VALUES ($1, $2, $3, $4) RETURNING id",
		usr.Username, usr.Email, usr.HashedPassword, usr.Salt,
	).Scan(&insertedID)
	if err != nil {
		return nil, fmt.Errorf("error inserting user in db: %w", err)
	}

	usr.ID = insertedID

	return &usr, nil
}

func (us userStoreImpl) GetByLoginHandle(ctx context.Context, login string) (*user.User, error) {
	if len(login) == 0 {
		return nil, user.ErrUserNotFound
	}
	usr := &user.User{}
	result := us.db.QueryRowContext(
		ctx,
		"SELECT id, username, email, hashed_password, salt, created_at, updated_at FROM users WHERE lower(email) = lower($1) or lower(username) = lower($2)",
		login, login,
	)
	if err := result.Scan(&usr.ID, &usr.Username, &usr.Email, &usr.HashedPassword, &usr.Salt, &usr.CreatedAt, &usr.UpdatedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, user.ErrUserNotFound
		}
		return nil, fmt.Errorf("error fetching record from db: %w", err)
	}
	return usr, nil
}

func (us userStoreImpl) LoginConflict(ctx context.Context, username, email string) (bool, error) {
	if len(username) == 0 && len(email) == 0 {
		return false, nil
	}

	var exists bool
	result := us.db.QueryRowContext(
		ctx,
		"SELECT TRUE FROM users WHERE lower(email) = lower($1) OR lower(username) = lower($2)",
		email, username,
	)
	if err := result.Scan(&exists); err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}

		return true, fmt.Errorf("error checking if user exists in db: %w", err)
	}
	return exists, nil
}
