package api

import (
	"context"
	"log/slog"
	"net/http"
	"strings"

	"github.com/kousikmitra/kitten-tracker/pkg/user"
)

type Middleware interface {
	Wrap(http.Handler) http.Handler
}

// MiddlewareFunc is like http.HandlerFunc, but for Middleware.
type MiddlewareFunc func(http.Handler) http.Handler

// Wrap implements Middleware.
func (f MiddlewareFunc) Wrap(w http.Handler) http.Handler {
	return f(w)
}

func WithAuthentication(userService user.Service) MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			headerVal := r.Header.Get("Authorization")
			token, found := strings.CutPrefix(headerVal, "Bearer")
			if !found {
				SendErrorResponse(w, http.StatusUnauthorized, "unathorized", "Expecting bearer token")
				return
			}

			token = strings.TrimSpace(token)

			usr, err := userService.Authenticate(r.Context(), token)
			if err != nil {
				slog.With("error", err).Error("unable to authenitcate with token")
				SendErrorResponse(w, http.StatusUnauthorized, "unathorized", "Invalid token")
				return
			}

			ctx := context.WithValue(r.Context(), user.AuthUserCtxKey, usr)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
