package api

import (
	"log/slog"
	"net/http"

	"github.com/kousikmitra/kitten-tracker/pkg/user"
)

func (s Server) CreateUser(w http.ResponseWriter, r *http.Request) {
	createUser := user.CreateUserCmd{}
	if err := BindJSON(r, &createUser); err != nil {
		slog.With("error", err).Error("unabled to decode request")
		SendErrorResponse(w, http.StatusBadRequest, "invlid_request", "expecting json request body")
		return
	}

	usr, err := s.UserService.CreateUser(r.Context(), createUser)
	if err != nil {
		slog.With("error", err).Error("unabled to create new user")
		// TODO: replace generic error
		SendErrorResponse(w, http.StatusUnprocessableEntity, "create_user_failed", err.Error())
		return
	}

	SendSuccessResponse(w, http.StatusCreated, usr)
}

func (s Server) LoginUser(w http.ResponseWriter, r *http.Request) {
	loginReq := user.LoginRequest{}
	if err := BindJSON(r, &loginReq); err != nil {
		slog.With("error", err).Error("unabled to decode request")
		SendErrorResponse(w, http.StatusBadRequest, "invlid_request", "expecting json request body")
		return
	}

	token, err := s.UserService.Login(r.Context(), loginReq)
	if err != nil {
		slog.With("error", err).Error("login user failed")
		// TODO: replace generic error
		SendErrorResponse(w, http.StatusUnprocessableEntity, "login_user_failed", err.Error())
		return
	}

	SendSuccessResponse(w, http.StatusOK, token)
}
