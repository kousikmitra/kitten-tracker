package api

import (
	"net/http"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/kousikmitra/kitten-tracker/pkg/animal"
	"github.com/kousikmitra/kitten-tracker/pkg/graph"
	"github.com/kousikmitra/kitten-tracker/pkg/user"
)

type Server struct {
	UserService user.Service

	handler    *http.ServeMux
	gqlHandler http.Handler
}

func NewServer(userService user.Service, animalService animal.Service) *Server {
	graphqlHandler := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: graph.NewResolver(userService, animalService)}))
	s := &Server{
		UserService: userService,

		handler:    http.DefaultServeMux,
		gqlHandler: graphqlHandler,
	}

	s.initRouter()

	return s
}

func (s *Server) initRouter() {
	s.handler.Handle("POST /api/user", http.HandlerFunc(s.CreateUser))
	s.handler.Handle("POST /api/login", http.HandlerFunc(s.LoginUser))
	s.handler.Handle("/", playground.Handler("GraphQL playground", "/query"))
	s.handler.Handle("/query", WithAuthentication(s.UserService).Wrap(s.gqlHandler))
}

func (s Server) Handler() http.Handler {
	return s.handler
}
