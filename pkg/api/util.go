package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ErrorResponse struct {
	Code    string `json:"code,omiempty"`
	Message string `json:"message,omiempty"`
}

func SendErrorResponse(w http.ResponseWriter, status int, code, message string) {
	w.WriteHeader(status)
	errResponse := ErrorResponse{code, message}
	if err := json.NewEncoder(w).Encode(errResponse); err != nil {
		panic(fmt.Errorf("unable to encode error response: %w", err))
	}
}

func SendSuccessResponse(w http.ResponseWriter, status int, data any) {
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(fmt.Errorf("unable to encode success response: %w", err))
	}
}

func BindJSON(r *http.Request, data any) error {
	if err := json.NewDecoder(r.Body).Decode(data); err != nil {
		return fmt.Errorf("invalid json request: %w", err)
	}
	return nil
}
