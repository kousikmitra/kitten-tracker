package email

import (
	"context"
	"fmt"
	"log/slog"
	"strings"
)

type Email struct {
	From    string
	To      []string
	Content string
}

type Mailer interface {
	Send(ctx context.Context, email Email) error
}

type logMailer struct {
	logger *slog.Logger
}

func NewLogMailer(logger *slog.Logger) Mailer {
	return &logMailer{logger.WithGroup("logmailer")}
}

func (lm logMailer) Send(ctx context.Context, email Email) error {
	lm.logger.Info(fmt.Sprintf("LogMailer: From: `%s`, To: `%s`, Content: `%s`", email.From, strings.Join(email.To, "; "), email.Content))
	return nil
}
