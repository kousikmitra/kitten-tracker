# kitten-tracker

### How to run the app?

> **With docker**

```bash
docker compose build
docker compose up
```

> **Without docker**

Requirements
* Postgres 13
* golang 1.22 
```bash
# Copy the sample.env to .env
cp sample.env .env

# Please update the server and db config if needed

# Create a database in postgres with `CREATE DATABASE kitten_tracker_dev;`

# Build go binary
make build

# Run migrations
./bin/kittens migrate

# Run the server
./bin/kittens server
```

### Tools and third party lib used

* Go 1.22
* Postgresql 13
* For implementing the graphql [99designs/gqlgen](https://github.com/99designs/gqlgen)
* For authentication using jwt [golang-jwt/jwt](https://github.com/golang-jwt/jwt)
* For database migration [golang-migrate/migrate](https://github.com/golang-migrate/migrate)

### Graphql APIs
You can use the graphiql server to explore the api spec.
* Create User
* Create Tiger
* Get All Tigers
* Add New Sighting
* Get All Aightings

Note: All graphql endpoints are authenticated. Please setup the `Authorization` header with `Bearer token` format.

### REST APIs
* Create User
```bash
curl -XPOST localhost:8080/api/user --data '{"password": "123", "email": "a@ab.coM"}'
```

* Login User
```bash
curl -XPOST localhost:8080/api/login --data '{"password": "123", "login": "a@ab.coM"}'
```

### How to run the tests?

> **With Docker**
Integration tests setup is done using docker, make sure your docker server is running.

```bash
make test
```

> **Without docker**
```bash
# Make sure local postgres instance is running
# Create a database in postgres with `CREATE DATABASE kitten_tracker_test;`

# Run the integration tests
go test ./tests -v
```
