FROM golang:1.22-alpine as build

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . ./

RUN go build -o kitten-tracker ./cmd/kittens

FROM alpine
WORKDIR /app
COPY --from=build /app/kitten-tracker ./
CMD [ "./kitten-tracker", "server" ]
EXPOSE 8080
