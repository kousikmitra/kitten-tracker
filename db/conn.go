package db

import (
	"database/sql"
	"fmt"

	"github.com/kousikmitra/kitten-tracker/pkg/config"
)

func NewDB(cfg config.DatabaseConfig) (*sql.DB, error) {
	connUri := buildDBConnURI(cfg)
	sqlDB, err := sql.Open("postgres", connUri)
	if err != nil {
		return nil, fmt.Errorf("error connecting to db: %w", err)
	}

	return sqlDB, nil
}

func buildDBConnURI(cnf config.DatabaseConfig) string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", cnf.Username, cnf.Password, cnf.Host, cnf.Port, cnf.Name)
}
