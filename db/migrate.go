package db

import (
	"database/sql"
	"embed"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/iofs"
)

//go:embed migration/*.sql
var migrationDIR embed.FS

const (
	MigrateDirectionUp int = iota
	MigrateDirectionDown
)

func MigrateUp(db *sql.DB) error {
	return migrateDB(db, MigrateDirectionUp)
}

func MigrateDown(db *sql.DB) error {
	return migrateDB(db, MigrateDirectionDown)
}

func migrateDB(db *sql.DB, direction int) error {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}

	fs, err := iofs.New(migrationDIR, "migration")
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("iofs", fs, "kitten_tracker_dev", driver)
	if err != nil {
		return err
	}
	switch direction {
	case MigrateDirectionUp:
		return m.Up()
	case MigrateDirectionDown:
		return m.Down()
	default:
		return fmt.Errorf("unknown migration direction: %d", direction)
	}
}
