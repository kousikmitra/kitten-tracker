CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  hashed_password VARCHAR(255),
  salt VARCHAR(255),
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  CONSTRAINT uniq_username UNIQUE(username),
  CONSTRAINT uniq_email UNIQUE(email)
);

CREATE TABLE animals (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50),
  date_of_birth TIMESTAMP NOT NULL,
  last_seen_at TIMESTAMP,
  last_loc_lat FLOAT,
  last_loc_lon FLOAT,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  CONSTRAINT lat_lon_both_required CHECK(
    (
      last_loc_lat IS NULL
      and last_loc_lon IS NULL
    )
    or (
      last_loc_lat IS NOT NULL
      and last_loc_lon IS NOT NULL
    )
  )
);
