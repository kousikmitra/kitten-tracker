CREATE TABLE animal_sightings (
  id SERIAL PRIMARY KEY,
  animal_id INTEGER REFERENCES animals,
  reported_by INTEGER REFERENCES users,
  lat FLOAT NOT NULL,
  lon FLOAT NOT NULL,
  ts TIMESTAMP DEFAULT NOW()
);
